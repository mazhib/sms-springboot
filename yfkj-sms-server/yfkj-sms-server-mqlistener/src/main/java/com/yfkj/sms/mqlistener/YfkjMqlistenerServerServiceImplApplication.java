package com.yfkj.sms.mqlistener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mzb
 */
@SpringBootApplication
public class YfkjMqlistenerServerServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(YfkjMqlistenerServerServiceImplApplication.class, args);
    }

}
