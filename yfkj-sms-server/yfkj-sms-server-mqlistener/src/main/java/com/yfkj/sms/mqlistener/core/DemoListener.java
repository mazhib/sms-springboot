package com.yfkj.sms.mqlistener.core;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * @author mzb
 * @version v1.0.0
 * @description
 * @date 2020/11/22 20:48
 */
@Service
@RocketMQMessageListener(topic = "demo-springboot", consumerGroup = "mayiktTopic")
public class DemoListener implements RocketMQListener {


    @Override
    public void onMessage(Object o) {
        System.out.println("接收到了消息" + JSON.toJSONString(o));
    }
}