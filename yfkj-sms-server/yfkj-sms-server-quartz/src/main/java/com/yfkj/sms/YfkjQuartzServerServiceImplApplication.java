package com.yfkj.sms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mzb
 */
@SpringBootApplication
@MapperScan("com.yfkj.sms.mapper")
public class YfkjQuartzServerServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(YfkjQuartzServerServiceImplApplication.class, args);
    }

}
