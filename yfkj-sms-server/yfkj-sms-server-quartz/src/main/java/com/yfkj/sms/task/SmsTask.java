package com.yfkj.sms.task;

import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 * 
 * @author mzb
 */
@Component("smsTask")
public class SmsTask
{
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println("执行有参方法多个类型："+s+b+l+i);
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }
}
