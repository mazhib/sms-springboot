package com.yfkj.sms.exception;

import com.yfkj.sms.domain.SysJob;
import com.yfkj.sms.util.AbstractQuartzJob;
import com.yfkj.sms.util.JobInvokeUtil;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author mzb
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
