package com.yfkj.sms.task;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author mzb
 * @version v1.0.0
 * @description
 * @date 2020/11/24 17:19
 */
@Slf4j
public class DemoJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
    log.info("定时任务demo开始执行");
    }
}
