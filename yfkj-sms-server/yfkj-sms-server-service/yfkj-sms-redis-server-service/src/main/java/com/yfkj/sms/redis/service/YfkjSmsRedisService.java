package com.yfkj.sms.redis.service;

import java.util.List;
import java.util.Map;

/**
 * @Description redis服务
 * @Author mzb
 * @Date 2020/11/25 16:54
 * @Version V1.0.0
 */
public interface YfkjSmsRedisService {


    /**
     * 方法作用描述
     * 保存String类型的数据
     * @author mzb
     * @param key :
     * @param value :
     * @return boolean
     */
    boolean setStr(String key, String value);

    /**
     * 方法作用描述
     * 保存string类型 带过期时间
     * @author mzb
     * @param key :
     * @param value :
     * @param expireTime :
     * @return boolean
     */
    boolean setStrExpire(String key, String value, Integer expireTime);

    /**
     * 方法作用描述
     *  取值 返回string 类型
     * @author mzb
     * @param key :
     * @return java.lang.String
     */
    String getVal(String key);

    boolean putVal(String key, Object value);

    boolean putVal(String nameSpace, String key, Object value);

    boolean putVal(String nameSpace, String key, Object value, Integer expireTime);

    String getVal(String nameSpace, String key);

    boolean existKey(String nameSpace, String key);

    boolean existKey(String key);

    boolean setCacheExpire(String key, int expireTime);

    boolean setCacheExpire(String nameSpace, String key, int expireTime);



    /**
     * 方法描述: 根据key删除指定的缓存（无空间）
     * @author fanzhengkai
     * @createDate 2020/6/10 22:11
     * @param key :
     * @return: java.lang.Boolean
     */
    Boolean del(String key);


    /**
     * 加锁
     *
     * @param key      - 唯一标志
     * @param value 当前时间+超时时间  必须是long类型时间 转String类型
     * @return true
     */
    boolean lock(String key, String value);

    /**
     * 解锁
     *
     * @param key   唯一标识
     * @param value 乐观锁版本号
     */
    void unLock(String key, String value);




}
