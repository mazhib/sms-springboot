package com.yfkj.sms.mongodb.service;


import java.util.List;
import java.util.Map;

/**
 * @Description mongodb服务
 * @Author mzb
 * @Date 2019/9/3 17:16
 * @Version V1.0.0
 */
public interface YfkjSmsMongoService {
    /**
     * 插入集合数据
     * @author       mzb
     * @description  插入集合数据
     * @date         2019/9/5 16:17
     * @param        col 集合名
     * @param        object 文档
     * @return       void
     **/
    void insertObjectToCol(Object object, String col);

    void insert(List<Object> object, String col);
    /**
     * 根据集合查询文档
     * @author       mzb
     * @description
     * @date         2019/9/5 16:34
     * @param        tClass
     * @param        col
     * @return       java.util.List<T>
     **/
    <T> List<T> findAll(Class<T> tClass, String col);
    /**
     * 分页查询
     * @author       mzb
     * @description
     * @date         2019/9/5 16:42
     * @param        col, clz, pageIndex,pageSize
     * @return       java.util.Map<java.lang.String,java.lang.Object>
     **/
    <T> Map<String, Object> findAllByPage(String col, Class<T> tClass, int pageIndex, int pageSize);

    long deleteMany(String col, String jsonOp);


}
