package com.yfkj.sms.service;

import java.util.List;
import java.util.Map;

/**
 * @ Author xsd
 * @ crate time 2020/11/22 16:12
 * @ 描述  es 基础服务  增删改查-
 */
public interface EsBaseService<T> {
    /**
     * 创建索引
     */
    void createIndex(Class<T> clazz) throws Exception;

    /**
     * 删除索引
     */
    void dropIndex(Class<T> clazz) throws Exception;

    /**
     *  单条插入
     */
    boolean insertById(T t) throws Exception;

    /**
     *  单条主键更新
     */
     boolean update(T t) throws Exception;

    /**
     *  单条主键查询
     * @param id 主键
     */
    T getById(String id, Class<T> clazz) throws Exception;

    /**
     * 获取全部
     */
    String getAll(Class<T> clazz) throws Exception;

    /**
     *  按字段查询
     */
    List<Map<String,Object>> search(String fileName, String value, Class<T> clazz) throws Exception;
    /**
     *  分页查询
     */
    Map<String,Object> queryByPage(int pageNo, int pageSize, String keyword, String indexName, String ... fieldNames);

    /**
     *  查询总数
     */
    long count( Class<T> clazz);
}
