package com.yfkj.sms.service;

/**
 * @author mzb
 * @version v1.0.0
 * @description mq  provider
 * @date 2020/11/22
 */
public interface RocketMqService {
    /**
     * 方法作用描述
     * 异步发送mq消息
     * @author  mzb
     * @date    2020/11/22 10:36
     * @param msg :   消息体
     * @param queue : 队列名
     */
    void sendMqMsg(String msg,String queue);


}
