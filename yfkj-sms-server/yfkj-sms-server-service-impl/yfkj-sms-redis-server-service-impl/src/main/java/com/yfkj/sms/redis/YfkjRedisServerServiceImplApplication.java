package com.yfkj.sms.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YfkjRedisServerServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(YfkjRedisServerServiceImplApplication.class, args);
    }

}
