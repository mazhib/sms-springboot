package com.yfkj.sms.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YfkjMongodbServerServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(YfkjMongodbServerServiceImplApplication.class, args);
    }

}
