package com.yfkj.sms.mongodb.service.impl;

import com.yfkj.sms.mongodb.service.YfkjSmsMongoService;
import org.apache.commons.lang.time.DateUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class YfkjSmsMongoServiceImplTest {

    @Resource
    private YfkjSmsMongoService yfkjSmsMongoService;

    @Test
    void insertObjectToCol() {
        DateUtils dateUtils = new DateUtils();
        yfkjSmsMongoService.insertObjectToCol(dateUtils,"mongTest");
    }

    @Test
    void insert() {
        List<DateUtils> a = new ArrayList<>();
        DateUtils dateUtils = new DateUtils();
        a.add(dateUtils);
        yfkjSmsMongoService.insert(Collections.singletonList(a),"mongTest1");
    }

    @Test
    void findAll() {
        yfkjSmsMongoService.findAll(DateUtils.class,"mongTest");
    }

    @Test
    void findAllByPage() {

        yfkjSmsMongoService.findAllByPage("mongTest",DateUtils.class,1,10);

    }

    @Test
    void deleteMany() {
        yfkjSmsMongoService.deleteMany("mongTest","");
    }
}