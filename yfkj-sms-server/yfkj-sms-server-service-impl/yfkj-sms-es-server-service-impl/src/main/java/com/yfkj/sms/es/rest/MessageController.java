package com.yfkj.sms.es.rest;

import com.yfkj.sms.es.model.Message;
import com.yfkj.sms.es.util.IndexTools;
import com.yfkj.sms.es.util.JsonUtil;
import com.yfkj.sms.es.util.MetaData;
import com.yfkj.sms.service.EsBaseService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ Author xsd
 * @ crate time 2020/11/22 18:37
 * @ 描述
 */
@RestController
@RequestMapping(value = "es")
public class MessageController {
    @Resource
    private EsBaseService<Message> esBaseService;

    @RequestMapping("createIndex")
    public String createIndex() throws Exception {
        esBaseService.createIndex(Message.class);
        return "ok...";
    }
    @RequestMapping("dropIndex")
    public String dropIndex() throws Exception {
        esBaseService.dropIndex(Message.class);
        return "ok...";
    }

    @RequestMapping("insertById")
    public String insertById() throws Exception {
        for (int i = 0; i < 20; i++) {
            Message message = new Message();
            message.setId(i + "");
            message.setAge(20 + i);
            message.setName1("宇芳科技" + i);
            esBaseService.insertById(message);
        }
        return "ok...";
    }

    @RequestMapping("get/{id}")
    public String deleteIndex(@PathVariable String id) throws Exception {
        Message message=esBaseService.getById(id, Message.class);
        return JsonUtil.toJson(message);
    }

    @RequestMapping("getAll")
    public String getAll() throws Exception {

        return esBaseService.getAll(Message.class);
    }

    @RequestMapping("search/{value}")
    public String search(String name,@PathVariable String value) throws Exception {

        return JsonUtil.toJson(esBaseService.search(name, value, Message.class));
    }

    @RequestMapping("count")
    public long count() throws Exception {
        return esBaseService.count(Message.class);
    }

    @RequestMapping("page")
    public String page() throws Exception {
        MetaData metaData = IndexTools.getIndexType(Message.class);
        String indexname = metaData.getIndexname();
        return JsonUtil.toJson(esBaseService.queryByPage(1, 3, "科技", indexname,"name1"));
    }
}
