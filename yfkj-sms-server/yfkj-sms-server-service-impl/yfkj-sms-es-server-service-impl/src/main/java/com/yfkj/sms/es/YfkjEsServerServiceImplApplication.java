package com.yfkj.sms.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ Author xsd
 * @ crate time 2020/11/22 16:39
 * @ 描述 es 服务
 */
@SpringBootApplication
public class YfkjEsServerServiceImplApplication {
    public static void main(String[] args) {
        SpringApplication.run(YfkjEsServerServiceImplApplication.class, args);
    }
}
