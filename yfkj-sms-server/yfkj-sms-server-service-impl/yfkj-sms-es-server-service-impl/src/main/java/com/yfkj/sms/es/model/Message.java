package com.yfkj.sms.es.model;

import com.yfkj.sms.es.annotation.ESID;
import com.yfkj.sms.es.annotation.ESMetaData;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

/**
 * @ Author xsd
 * @ crate time 2020/11/22 18:31
 * @ 描述
 */
@Data
@Document(indexName = "yfkj_message", type = "yfkj_message")
@ESMetaData(indexName = "yfkj_message",indexType = "yfkj_message", number_of_shards = 5,number_of_replicas = 5,printLog = true)
public class Message implements Serializable {
    @ESID
    private String id;

    private String name1;

    private int age;
}
