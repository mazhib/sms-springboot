package com.yfkj.sms.es.annotation;

/**
 * 聚合metric类型枚举
 **/
public enum AggsType {
    //-
    sum,min,max,count,avg
}
