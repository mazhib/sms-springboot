package com.yfkj.sms.es.annotation;

/**
 *  es字段数据结构
 **/
public enum DataType {
    //-
    keyword_type,text_type,byte_type,short_type,integer_type,long_type,float_type,double_type,boolean_type,date_type;
}
