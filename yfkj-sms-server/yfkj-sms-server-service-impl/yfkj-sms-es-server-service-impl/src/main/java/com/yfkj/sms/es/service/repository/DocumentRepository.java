package com.yfkj.sms.es.service.repository;

import com.yfkj.sms.es.model.Message;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 *  继承  ElasticsearchRepository bean、需要针对每个实体实现。扩展性差。
 *  ElasticsearchRepository _saveAll()\deleteById()\deleteAll()\findById()\findAll()
 */
@Component
public interface DocumentRepository extends ElasticsearchRepository<Message,String> {
}
