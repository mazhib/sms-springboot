package com.yfkj.sms.rocketmq.service.impl;

import com.yfkj.sms.service.RocketMqService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RocketMqServiceImplTest {

//    @Resource
//    private RocketMqService rocketMqService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Test
    public void sendMqMsg() {
        String queue = "demo-springboot";
        String msg = "【宇芳科技】尊敬的用户您好，这里是山西宇芳物联，诚邀您参加11月22号亚太经济峰会";
        rocketMQTemplate.convertAndSend(queue,msg);

    }
}