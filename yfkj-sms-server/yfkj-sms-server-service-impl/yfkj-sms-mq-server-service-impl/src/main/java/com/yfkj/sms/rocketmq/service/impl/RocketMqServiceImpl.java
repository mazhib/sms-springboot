package com.yfkj.sms.rocketmq.service.impl;

import com.yfkj.sms.service.RocketMqService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;

import javax.annotation.Resource;

/**
 * @author mzb
 * @version v1.0.0
 * @description 模板mq发送消息
 * @date 2020/11/22 10:32
 */
@DubboService
public class RocketMqServiceImpl implements RocketMqService {

    @Resource
    private RocketMQTemplate rocketMqTemplate;

    @Override
    public void sendMqMsg(String msg, String queue) {
        rocketMqTemplate.convertAndSend(queue,msg);
    }
}
