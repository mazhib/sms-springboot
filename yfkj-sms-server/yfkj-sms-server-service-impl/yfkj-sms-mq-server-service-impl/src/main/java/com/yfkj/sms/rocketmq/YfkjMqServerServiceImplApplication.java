package com.yfkj.sms.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mzb
 */
@SpringBootApplication
public class YfkjMqServerServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(YfkjMqServerServiceImplApplication.class, args);
    }

}
